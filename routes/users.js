const express = require('express');
const router = express.Router();
var bcrypt = require('bcryptjs');
const  passport = require('passport');

//Login Page
router.get('/login',(req,res) => res.render('login'));

//User model
const User = require('../models/User');


//Register Page
router.get('/register',(req,res) => res.render('register'));

//Register Handle
router.post('/register',(req,res) =>{
    const { firstname,lastname,email,password,password2 } = req.body;
    let errors = [];
    //check required fields
    if(!firstname || !lastname|| !email || !password || !password2){
        errors.push({msg: 'please fill in all fields'});
    }

    //check password match
    if(password !== password2){
        errors.push({msg: 'Passwords do not match'});
    }

    //check pass lenght
    if(password.length < 6){
        errors.push({msg: 'Password must be at least 6 characters'});
    }

    if(errors.length >0){
        res.render('register',{
            errors,
            firstname,
            lastname,
            email,
            password,
            password2
        });
        
    }else {
        //validate passed
        User.findOne({email:email})
        .then(user => {
            if(user) {
                //User exists 
                errors.push({msg:'Email is already registered'})
            res.render('register',{
                errors,
                firstname,
                lastname,
                email,
                password,
                password2
            });
        } else {
            const newUser = new User({
                firstname,
                lastname,
                email,
                password
            });
            //Hash Password
            bcrypt.genSalt(10, (err,salt) =>
            bcrypt.hash(newUser.password,salt, (err,hash) =>{
                if(err) throw err;

                //set password to hashed
                newUser.password = hash;
                //save User
                newUser.save()
                    .then(user => {
                        res.redirect('login');
                    })
                    .catch(err => console.log(err));
            }))
        }
        });
    }

} );

//Login
router.post('/login', (req,res,next) =>{
    passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/users/login',
        failureFlash:true
    })(req,res,next);
});

//LogOut
router.get('/logout',(req,res) =>{
    req.logout();
    req.flash('success_msg','You are logged out');
    res.redirect('/users/login');
});

module.exports = router;