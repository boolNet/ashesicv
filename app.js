const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const officegen = require('officegen');
const fs = require('fs');


var app = express();

//Passport Config
require('./config/passport')(passport);

//DB Config
const db = require('./config/keys.js').MongoURI;
require('./config/passport');

//This is just to test the word writing module(officegen)
require('./models/word');

//Connect to Mongo
const authData = {
    'useNewUrlParser': true 
};
mongoose.connect(db,authData)
    .then(() => console.log('MonogoDB connected'))
    .catch(err => console.log(err));



//Setting template engine
app.use(expressLayouts);
app.set('view engine','ejs');

//BodyParser
app.use(express.urlencoded({ extended:false }));

//Express Sessions
app.use(session({
    secret: 'secret',
    saveUninitialied: true,
    resave: true
  }));

//Passport MiddleWare
app.use(passport.initialize());
app.use(passport.session());

//Connect flash
app.use(flash());

//Global Variables
// Global variables
app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
  });

//Routes
app.use('/',require('./routes/index.js'));
app.use('/users',require('./routes/users.js'));




// //Static files
// app.use(express.static('./public'));


// //fire controlller
// middlewares(app);

// //flash for login success and error
// app.use(flash());
// app.use((req,res,next) =>{
//     res.locals.success_message = req.flash('success');
//     res.locals.error_message = req.flash('success');
//     next();
// })


//listen to port
app.listen(4000);
console.log("You are listening at port: " + 4000);