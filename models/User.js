var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var UserSchema = new Schema({
    firstname:{
        type:String,
        required:true,
        trim:true
    },
    lastname:{
        type:String,
        required:true,
        trim:true
    },
    email: {
        type: String,
        unique:true,
        required:true,
        trim:true
    },
    password: {
        type:String,
        required:true,

    },
    date:{
        type:Date,
        default:Date.now
    }
});



const User = mongoose.model('User',UserSchema);
module.exports = User;

