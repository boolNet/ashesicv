const officegen = require('officegen');
const fs = require('fs');



const docx = officegen({
    'type': 'docx',
    'subject':'Example1'
});

//Creating a paragraph
var pObj = docx.createP();

//Adding a text
pObj.addText ( 'Simple' );
 
pObj.addText ( ' with color', { color: '000088' } );
 
pObj.addText ( ' and back color.', { color: '00ffff', back: '000088' } );
 
pObj.addText ( 'Bold + underline', { bold: true, underline: true } );
 
pObj.addText ( 'Fonts face onlyff.', { font_face: 'Arial' } );
pObj.addHorizontalLine();
pObj.addHorizontalLine();
 
pObj.addText ( ' Fonts face and size. ', { font_face: 'Arial', font_size: 40 } );
 
pObj.addText ( 'External link', { link: 'https://github.com' } );
 
// Hyperlinks to bookmarks also supported:
pObj.addText ( 'Internal link', { hyperlink: 'myBookmark' } );
// ...
// Start somewhere a bookmark:
pObj.startBookmark ( 'myBookmark' );
// ...
// You MUST close your bookmark:
pObj.endBookmark ();


const out = fs.createWriteStream('firstword.docx');

const closed = () =>console.log('Finished creating');
out.on('close',closed);
docx.generate(out);

